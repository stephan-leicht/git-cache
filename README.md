# git-cache

Git Cache creates a cache of git objects that can be used when checking
out projects from git repositories. Objects already in 
the cache do not need to be downloaded again allowing for speedier clones.

## Usage

For example, if a git repository is already in the cache (or most of the commits) you can
use the reference option like:

git clone --reference ~/.gitcache git@gitlab.com:stephan-leicht/git-cache.git

All the objects that are in the reference will be fetched from there rather
than downloaded. Fetching locally is much faster than downloading them.

This was inspired by Randy Fays scripts at http://randyfay.com/node/93

#### Initialize:
	git cache init <REPO_URL> <CACHE_DIR>

 * REPO_URL   - URL of git repository to mirror.
 * CACHE_DIR  - Location of cache inside filesystem.

#### Update repos:
	git cache update <CACHE_DIR>

 * CACHE_DIR  - Location of cache inside filesystem.

#### Show remote repos:
	git cache show <CACHE_DIR>

 * CACHE_DIR  - Location of cache inside filesystem.

#### Clone repo with cache:
	git cache clone <REPO_URL> <CACHE_DIR> <LOCAL_REPO_DIR> (<LOCAL_REPO_NAME>)

 * REPO_URL   - URL of git repository to mirror.
 * CACHE_DIR  - Location of cache inside filesystem.
 * LOCAL_REPO_DIR - Location of cloned/checkedout repository.
 * LOCAL_REPO_NAME - Overwrites default repository name

#### Use cache on present local repo:
	git cache use <CACHE_DIR> <LOCAL_REPO_DIR>

 * CACHE_DIR  - Location of cache inside filesystem.
 * LOCAL_REPO_DIR - Location of cloned/checkedout repository.

## Install

#### 1: Will be installed into $HOME/.bin for OSX $HOME/bin for Linux and MINGW(Git Bash on Windows)
    curl -L https://gitlab.com/stephan-leicht/git-cache/raw/master/bin/install | bash
#### 2: _download_ and run it yourself
	curl -L https://gitlab.com/stephan-leicht/git-cache/raw/master/bin/install > install && chmod +x install && ./install
#### 3: _download_ and run it yourself
    export BIN_DIR=/usr/local/bin && curl -L https://gitlab.com/stephan-leicht/git-cache/raw/master/bin/install | bash